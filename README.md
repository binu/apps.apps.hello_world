# biNu Hello World #

Welcome to biNu's hello world examples. These files demonstrate the minimum you need to get some basic screens working in biNu.

To run the examples:

 * Register a developer account at [Dev Central](hhttp://developer.binu.com/)
 * Clone this repo and place the files in a public web accessible directory
 * Register a new app in Dev Central, and point it to one of the example files
 * To run in the Dev Central phone emulator click "list apps" then "test app"

## Files ##

The language is BML (biNu Markup Language) The examples here are implemented as flat XML files, however they could also be created using the language of your choice, so long as the output is structured XML.

### Hello World ###

This is about the simplest page you could create in BML. It just prints "Hello World" to the screen

### Hello Solar System ###

A bit bigger than Hello World, but still a simple example of a BML page. Here we've added some additional styles, some text to use a style, and some navigation links in the bottom nav bar.

### Hello Gallaxy ###

A bit bigger again than Hello Solar System. This time we've added an aditional page segment, a popup menu, and a link.